from thermo.mixture import Mixture
from thermo_extra.eos_mix import (
    # AS
    PRASMIX,
    SRKASMIX,
    # SIL
    PRSILMIX,
    SRKSILMIX,
    # AS + SIL
    PRASILMIX,
    SRKASILMIX,
    MSLMIX
)
import numpy as np


def test_PRASMIX():
    z = 0.0556
    IDs = ["aceton", "carbon tetrachloride"]
    mix = Mixture(IDs, zs=[z, 1-z], T=318.15, P=42039.2092)
    l, m = [0.054802382277003465, -0.014207771305196055]
    lijs = [[0, l], [l, 0]]
    mijs = [[0, m], [m, 0]]
    eos = PRASMIX(mix.Tcs, mix.Pcs, mix.omegas, mix.zs,
                  lijs=lijs, mijs=mijs, T=mix.T, P=mix.P)

    rzs = np.divide(eos.phis_l, eos.phis_g) * eos.zs

    assert np.allclose(np.sum(rzs), 0.9849211069207698)


def test_SRKASMIX():
    z = 0.0556
    IDs = ["aceton", "carbon tetrachloride"]
    mix = Mixture(IDs, zs=[z, 1-z], T=318.15, P=42039.2092)
    l, m = [0.054802382277003465, -0.014207771305196055]
    lijs = [[0, l], [l, 0]]
    mijs = [[0, m], [m, 0]]
    eos = SRKASMIX(mix.Tcs, mix.Pcs, mix.omegas, mix.zs,
                   lijs=lijs, mijs=mijs, T=mix.T, P=mix.P)

    rzs = np.divide(eos.phis_l, eos.phis_g) * eos.zs

    assert np.allclose(np.sum(rzs), 0.9543884669396618)


def test_PRSILMIX():
    z = 0.0556
    IDs = ["aceton", "carbon tetrachloride"]
    mix = Mixture(IDs, zs=[z, 1-z], T=318.15, P=42039.2092)
    k = 0.0641753392758043
    kijs = [[0, k], [k, 0]]
    eos = PRSILMIX(mix.Tcs, mix.Pcs, mix.Zcs, mix.omegas, mix.zs,
                   kijs=kijs, T=mix.T, P=mix.P)

    rzs = np.divide(eos.phis_l, eos.phis_g) * eos.zs

    assert np.allclose(np.sum(rzs), 0.3569901108992495)


def test_SRKSILMIX():
    z = 0.0556
    IDs = ["aceton", "carbon tetrachloride"]
    mix = Mixture(IDs, zs=[z, 1-z], T=318.15, P=42039.2092)
    k = 0.0641753392758043
    kijs = [[0, k], [k, 0]]
    eos = SRKSILMIX(mix.Tcs, mix.Pcs, mix.Zcs, mix.omegas, mix.zs,
                    kijs=kijs, T=mix.T, P=mix.P)

    rzs = np.divide(eos.phis_l, eos.phis_g) * eos.zs

    assert np.allclose(np.sum(rzs), 0.07964601348113283)


def test_PRASILMIX():
    z = 0.0556
    IDs = ["aceton", "carbon tetrachloride"]
    mix = Mixture(IDs, zs=[z, 1-z], T=318.15, P=42039.2092)
    l, m = [0.054802382277003465, -0.014207771305196055]
    lijs = [[0, l], [l, 0]]
    mijs = [[0, m], [m, 0]]
    eos = PRASILMIX(mix.Tcs, mix.Pcs, mix.Zcs, mix.omegas, mix.zs,
                    lijs=lijs, mijs=mijs, T=mix.T, P=mix.P)

    rzs = np.divide(eos.phis_l, eos.phis_g) * eos.zs

    assert np.allclose(np.sum(rzs), 0.3541900894768789)


def test_SRKASILMIX():
    z = 0.0556
    IDs = ["aceton", "carbon tetrachloride"]
    mix = Mixture(IDs, zs=[z, 1-z], T=318.15, P=42039.2092)
    l, m = [0.054802382277003465, -0.014207771305196055]
    lijs = [[0, l], [l, 0]]
    mijs = [[0, m], [m, 0]]
    eos = SRKASILMIX(mix.Tcs, mix.Pcs, mix.Zcs, mix.omegas, mix.zs,
                     lijs=lijs, mijs=mijs, T=mix.T, P=mix.P)

    rzs = np.divide(eos.phis_l, eos.phis_g) * eos.zs

    assert np.allclose(np.sum(rzs), 0.07889576687181094)


def test_MSLMIX():
    z = 0.0556
    IDs = ["aceton", "carbon tetrachloride"]
    mix = Mixture(IDs, zs=[z, 1-z], T=318.15, P=42039.2092)
    l, m = [0.054802382277003465, -0.014207771305196055]
    lijs = [[0, l], [l, 0]]
    mijs = [[0, m], [m, 0]]
    eos = MSLMIX(mix.Tcs, mix.Pcs, mix.Zcs, mix.zs, names=IDs,
                 lijs=lijs, mijs=mijs, T=mix.T, P=mix.P)

    rzs = np.divide(eos.phis_l, eos.phis_g) * eos.zs

    assert np.allclose(np.sum(rzs), 0.9997084013011546)
