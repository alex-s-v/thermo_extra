from thermo.mixture import Chemical
from thermo_extra.eos import PRSIL, SRKSIL
import numpy as np


def test_PRSIL():
    chem = Chemical("aceton", T=318.15, P=42039.2092)
    prsil = PRSIL(
        Tc=chem.Tc,
        Pc=chem.Pc,
        Zc=chem.Zc,
        omega=chem.omega,
        T=chem.T,
        P=chem.P
    )

    V_g = 0.06183281567140182
    V_l = 7.065379338697571e-05

    assert np.allclose([prsil.V_g, prsil.V_l], [V_g, V_l])


def test_SRKSIL():
    chem = Chemical("aceton", T=318.15, P=42039.2092)
    srksil = SRKSIL(
        Tc=chem.Tc,
        Pc=chem.Pc,
        Zc=chem.Zc,
        omega=chem.omega,
        T=chem.T,
        P=chem.P
    )

    V_g = 0.06177906684343868
    V_l = 6.965557086918242e-05

    assert np.allclose([srksil.V_g, srksil.V_l], [V_g, V_l])
