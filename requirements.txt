fluids==0.1.77
numpy==1.18.0
pandas==0.25.3
python-dateutil==2.8.1
pytz==2019.3
scipy==1.4.1
six==1.13.0
thermo==0.1.39
