import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

with open("requirements.txt", "r") as f:
    install_requires = f.read().strip().split()

keywords = """chemical engineering chemistry mechanical engineering
thermodynamics databases cheminformatics engineering viscosity
density heat capacity thermal conductivity surface tension
combustion environmental engineering solubility vapor pressure
equation of state molecule"""

setuptools.setup(
    name="thermo-extra",
    version="0.0.1",
    author="Alexansdr Vasilyev",
    author_email="alexandr.s.vasilyev@gmail.com",
    description="Extention for the Caleb Bell's thermo project (https://github.com/CalebBell/thermo)",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/alex-s-v/extra_eos",
    packages=setuptools.find_packages(
        include=["thermo_extra", "thermo_extra.*"],
        exclude=["*.tests", "*.tests.*", "tests.*", "tests"]
    ),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        'Intended Audience :: Developers',
        'Intended Audience :: Education',
        'Intended Audience :: Manufacturing',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Topic :: Education',
        'Topic :: Scientific/Engineering :: Atmospheric Science',
        'Topic :: Scientific/Engineering :: Chemistry',
        'Topic :: Scientific/Engineering :: Physics',
    ],
    keywords=keywords,
    python_requires=">=3.7",
    install_requires=install_requires,
    package_data={
        "thermo_extra": ["VaporPressure/*"]
    }
)
