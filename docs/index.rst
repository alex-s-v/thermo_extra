.. thermo-extra documentation master file, created by
   sphinx-quickstart on Tue Nov  5 22:00:35 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to thermo-extra's documentation!
========================================

.. toctree::
   :maxdepth: 10

   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
