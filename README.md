# thermo-extra

## What is thermo?

thermo-extra is extension to the great project of
Caleb Bell called [thermo](https://github.com/CalebBell/thermo).

The reason that this package is not part of the main
thermo project is that I want more freedom
in my code, but for the most part code of this package will
follow the rules of the main project.

Currently thermo-extra is in the early development and some major changes can occur.

# Documentaton

Documentation is available on:

[https://thermo-extra.readthedocs.io](https://thermo-extra.readthedocs.io)

## Installation

To get the git version, run:

    $ python -m pip install git+https://alex-s-v@bitbucket.org/alex-s-v/thermo_extra.git

## Main goal

The main goal for thermo-extra is to provide extra functionality for Caleb Bell's thermo package.