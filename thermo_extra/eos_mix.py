import numpy as np
from scipy.optimize import fsolve
from thermo.utils import R
from thermo.identifiers import CAS_from_any
from thermo.eos_mix import PRMIX, SRKMIX
from thermo_extra.eos import PRSIL, SRKSIL

from thermo_extra.vapor_pressure import VaporPressure, ANTOINE_YAWS

__all__ = ["MSLMIX", "PRASMIX", "SRKASMIX", "PRSILMIX",
           "SRKSILMIX", "PRASILMIX", "SRKASILMIX"]


class GCASMIX(object):
    r"""Provides general functionality for Adachi-Sugie mixing rule.

    .. math::
        k_{ij} = l_{ij} + m_{ij} (z_i - z_j)

    Parameters
    ----------
    Tcs : list of float
        Critical temperatures of all compounds [K].
    zs : list of float
        Overall mole fractions of all compounds.
    lijs : list of list of float
        N*N size list of lists with first binary interaction parameters
        for the Adachi-Sugie mixing rule.
    mijs : list of list of float
        N*N size list of lists with second binary interaction parameters
        for the Adachi-Sugie mixing rule.

    Notes
    -----
    This class is useless on its own but can be used to add
    its functionality to other classes through inheritance.

    References
    ----------
    .. [1] Adachi Y., Sugie H. A new mixing rule-modified conventional mixing
       rule // Fluid Phase Equilib. 1986. Vol. 28, № 2. P. 103–118.
    """
    def __init__(self, Tcs, zs, lijs=None, mijs=None):
        N = len(Tcs)
        zszs = np.array([zs] * N)
        self._zmz = zszs.T - zszs
        nn = [[0] * N] * N
        self.lijs = nn if lijs is None else lijs
        self.mijs = nn if mijs is None else mijs

    @property
    def lijs(self):
        return self._lijs

    @lijs.setter
    def lijs(self, lijs):
        self._lijs = np.array(lijs)
        if hasattr(self, "_mijs"):
            self.kijs = self._lijs + self._mijs * (self._zmz)

    @property
    def mijs(self):
        return self._mijs

    @mijs.setter
    def mijs(self, mijs):
        self._mijs = np.array(mijs)
        if hasattr(self, "_lijs"):
            self.kijs = self._lijs + self._mijs * (self._zmz)


class MSLMIX(object):
    r"""The representation of a MSL EoS [1].
    This EoS uses two parametric mixing rule [2].

    Parameters
    ----------
    Tcs : ndarray
        Critical temperatures of individual chemicals, [K].
    Pcs : ndarray
        Critical pressures of individual chemicals, [Pa].
    Zcs : ndarray
        Critical compressibility factor of individual
        chemicals.
    zs : ndarray
        Molar fractions of individual chemicals.
    T : float
        Temperature, [K].
    P : float
        Pressure, [Pa].
    lijs : ndarray
        Interaction parameter for Adachi-Sugie mixing rule.
    mijs : ndarray
        Interaction parameter for Adachi-Sugie mixing rule.

    References
    ----------
    .. [1] Margerum M.R., Sugie H., Lu B.C.Y. Equation of
       State for Polar and Nonpolar Substances from
       Correlations of Pure Component Properties //
       Chem. Eng. Commun. 1990. Vol. 92, № 1. P. 153–167.
    .. [2] Adachi Y., Sugie H. A new mixing rule-modified
       conventional mixing rule // Fluid Phase Equilib.
       1986. Vol. 28, № 2. P. 103–118.
    """
    alpha_xtol = 1e-8

    def __init__(self, Tcs, Pcs, Zcs, zs, T=298.15, P=101325,
                 lijs=None, mijs=None, names=None, cass=None,
                 Psat_method=ANTOINE_YAWS, vap_pressure=VaporPressure,
                 **kwargs):
        self.N = len(Tcs)
        self.zs = np.array(zs)
        self._zszs = np.array([self.zs] * self.N)
        self._zz = self._zszs.T * self._zszs
        self._zmz = self._zszs.T - self._zszs
        self._z2mz = 2*self._zszs.T - self._zszs
        self.Zcs = np.array(Zcs)
        self.Tcs = np.array(Tcs)
        self.Pcs = np.array(Pcs)
        self.T = T
        self.P = P
        Cas, Cbs = [], []
        rs, ss = [], []
        fs, gs, hs = [], [], []
        for Zc in Zcs:
            ca, cb, r, s, f, g, h = self.calc_consts(Zc)
            Cas.append(ca)
            Cbs.append(cb)
            rs.append(r)
            ss.append(s)
            fs.append(f)
            gs.append(g)
            hs.append(h)
        if lijs is None:
            lijs = [[0] * self.N] * self.N
        if mijs is None:
            mijs = [[0] * self.N] * self.N
        self.lijs = lijs
        self.mijs = mijs
        self.Cas = np.array(Cas)
        self.Cbs = np.array(Cbs)
        self.rs = np.array(rs)
        self.ss = np.array(ss)
        self.fs = np.array(fs)
        self.gs = np.array(gs)
        self.hs = np.array(hs)
        self._om3f = 1 - 3*self.fs
        self._f3 = self.fs**3
        self.hmg = self.hs - self.gs
        self.gmf = self.gs - self.fs
        self.hmf = self.hs - self.fs
        self.ais = self.Cas * (R * self.Tcs)**2 / self.Pcs
        self.bis = self.Cbs * (R * self.Tcs) / self.Pcs
        self.Psat_eqs = []
        if names is not None and Psat_method == ANTOINE_YAWS:
            cass = [CAS_from_any(name) for name in names]
        if cass is not None:
            for i, cas in enumerate(cass):
                vp = vap_pressure(
                    Tc=Tcs[i],
                    Pc=Pcs[i],
                    CASRN=cas
                )
                self.Psat_eqs.append(_wrap_method(vp, Psat_method))
        elif names is not None:
            for i, name in enumerate(names):
                vp = vap_pressure(
                    Tc=Tcs[i],
                    Pc=Pcs[i],
                    name=name
                )
                self.Psat_eqs.append(_wrap_method(vp, Psat_method))
        if len(self.Psat_eqs) == self.N:
            self.solve()

    @property
    def lijs(self):
        return self._lijs

    @lijs.setter
    def lijs(self, lijs):
        self._lijs = np.array(lijs)
        self._oml = 1 - self._lijs
        if hasattr(self, "_mz"):
            self._omlz = (self._oml - self._mz) * self._zz
        if hasattr(self, "mijs"):
            self._omlz2 = self.zs * (self._oml - self.mijs * self._z2mz)

    @property
    def mijs(self):
        return self._mijs

    @mijs.setter
    def mijs(self, mijs):
        self._mijs = np.array(mijs)
        self._mz = self._mijs * self._zmz
        self._mz2 = self._mz * self._zz
        if hasattr(self, "_oml"):
            self._omlz = (self._oml - self._mz) * self._zz
            self._omlz2 = self.zs * (self._oml - self.mijs * self._z2mz)

    @classmethod
    def calc_consts(cls, Zc):
        """Calculate constants for this EoS.
        Parameters
        ----------
        Zc : float
            Critical compressibility factor.
        Returns
        -------
        ca : float
            Value of A at Tc and Pc.
        cb : float
            Value of B at Tc and Pc.
        r : float
            Coefficient in critical point expression
            for `g`.
        s : float
            Coefficient in critical point expression
            for `h`.
        f : float
            Parameter f = Zc - ca.
        g : float
            Parameter g = Zc - r*ca.
        h : float
            Parameter h = Zc - s*ca.
        """
        def of(ca):
            cb = (1-ca**(1/3))**1.5
            return Zc - cb - 1 + ca**(1/3)

        ca = fsolve(of, 0.5)[0]
        cb = (1-ca**(1/3))**1.5

        f = Zc - cb
        g = (ca**(1/3) + (4*ca - 3*ca**(2/3))**0.5)/2
        h = (ca**(1/3) - (4*ca - 3*ca**(2/3))**0.5)/2
        u = (1 - 3*f) / f**1.5 - 2

        r = (-u - (4*u+u**2)**0.5)/2
        s = (-u + (4*u+u**2)**0.5)/2

        return ca, cb, r, s, f, g, h

    def solve(self):
        """Solve EoS for specified parameters.
        """
        A, B, B2, B3, A_is, B_is, B2_is, B3_is = self.calc_params()

        h1 = B2 * B3
        h2 = B + 1
        h3 = B3 + B2
        Zs = np.roots([1, -(h3+h2), (h1+h2*h3+A), -(h1*h2+A*B)])
        Zv = max(Zs)
        Zl = min(Zs)

        phis = []
        h3 = (B2_is - B3_is) / (B2 - B3) - A_is / A - 1
        h4 = A / (B2 - B3)
        for Z in [Zl, Zv]:
            h1 = B_is / (Z - B)
            h2 = np.log(Z - B)
            h5 = np.log((Z - B3)/(Z - B2))
            h6 = B2_is / (Z - B2) - B3_is / (Z - B3)
            phi = np.exp(h1 - h2 + h3 * h4 * h5 - h4 * h6)
            phis.append(phi)

        self.Z_l = Zl
        self.Z_g = Zv
        self.V_l = Zl * R * self.T / self.P
        self.V_g = Zv * R * self.T / self.P
        self.phis_l = phis[0]
        self.phis_g = phis[1]
        return None

    def fugacities(self):
        """For compatibility with `thermo.eos_mix`.
        """
        return None

    def calc_lambdas(self, PTr, alphaTr, i):
        """Calculate lambdas for solving expression
        for alpha.
        Parameters
        ----------
        PTr : float
            Reduced pressure devided by reduced temperature.
        alphaTr : float
            Alpha devided by reduced temperature.
        Returns
        -------
        lambdaL : float
            Lambda for liquid phase.
        lambdaV : float
            Lambda for vapor phase.
        """
        S0 = PTr**2 * self._f3[i]
        S1 = S0 - PTr*(self._om3f[i] - self.Cas[i]*alphaTr)
        S2 = PTr * self._om3f[i] - 1

        lambdas = np.roots([1, S2, S1, -S0])
        lambdaL = min(lambdas)
        lambdaV = max(lambdas)

        return lambdaL, lambdaV

    def calc_alpha(self):
        """Calculate alpha coefficient.
        Returns
        -------
        alpha : ndarray
            Alpha coefficient for calculation of A.
        """
        Ps = [Psat_eq(self.T) for Psat_eq in self.Psat_eqs]
        Prs = np.divide(Ps, self.Pcs)
        Trs = self.T / self.Tcs
        PTrs = Prs / Trs
        alphaTrs_init = 1 - self.fs / (1 - self.fs) * np.log(PTrs)

        def of(alphaTr, PTr, i):
            lL, lV = self.calc_lambdas(PTr, alphaTr, i)
            h2 = lV + PTr*self.gmf[i]
            h3 = lL + PTr*self.hmf[i]
            h4 = lV + PTr*self.hmf[i]
            h5 = lL + PTr*self.gmf[i]
            h6 = (lL - lV) + np.log(lV / lL)
            top = self.hmg[i]*h6
            bot = self.Cas[i] * np.log((h2*h3) / (h4*h5))
            res = top / bot - alphaTr
            return res

        alphas = []
        for i, atr_init in enumerate(alphaTrs_init):
            alphaTr = fsolve(
                of,
                alphaTrs_init[i],
                args=(PTrs[i], i),
                xtol=self.alpha_xtol
            )[0]
            alphas.append(alphaTr * Trs[i])
        return alphas

    def calc_params(self):
        """Calculate parameters for solving expression
        for fugacity coefficient.
        Returns
        -------
        A, B, B2, B3: float
        A_is, B_is, B2_is, B3_is : ndarray
        """
        aalpha = self.ais * self.calc_alpha()
        Ais = aalpha * self.P / (R * self.T)**2
        B_is = self.bis * self.P / (R * self.T)
        B2_is = self.rs * B_is
        B3_is = self.ss * B_is

        AiAi = np.array([Ais] * self.N)
        AA = (AiAi.T * AiAi)**0.5
        A = np.sum(AA * self._omlz)

        B = np.sum(self.zs * B_is)
        B2 = np.sum(self.zs * B2_is)
        B3 = np.sum(self.zs * B3_is)

        A_is = 2*np.sum(AA * self._omlz2, axis=1)
        A_is += np.sum(AA * self._mz2) - A

        return A, B, B2, B3, A_is, B_is, B2_is, B3_is


class PRASMIX(PRMIX, GCASMIX):
    r"""Class for solving the Peng-Robinson cubic equation of state for a
    mixture of any number of compounds. Subclasses `PR`. Solves the EOS on
    initialization and calculates fugacities for all components in all phases.

    The implemented method here is `fugacity_coefficients`, which implements
    the formula for fugacity coefficients in a mixture as given in [1]_.
    Two of `T`, `P`, and `V` are needed to solve the EOS.

    This implementation uses Adachi-Sugie mixing rule.

    .. math::
        P = \frac{RT}{v-b}-\frac{a\alpha(T)}{v(v+b)+b(v-b)}

        a \alpha = \sum_i \sum_j z_i z_j {(a\alpha)}_{ij}

        k_{ij} = l_{ij} + m_{ij} (z_i - z_j)

        (a\alpha)_{ij} = (1-k_{ij})\sqrt{(a\alpha)_{i}(a\alpha)_{j}}

        b = \sum_i z_i b_i

        a_i=0.45724\frac{R^2T_{c,i}^2}{P_{c,i}}

        b_i=0.07780\frac{RT_{c,i}}{P_{c,i}}

        \alpha(T)_i=[1+\kappa_i(1-\sqrt{T_{r,i}})]^2

        \kappa_i=0.37464+1.54226\omega_i-0.26992\omega^2_i

    Parameters
    ----------
    Tcs : list of float
        Critical temperatures of all compounds, [K]
    Pcs : list of float
        Critical pressures of all compounds, [Pa]
    omegas : list of float
        Acentric factors of all compounds, [-]
    zs : list of float
        Overall mole fractions of all species, [-]
    lijs : list of list of float
        N*N size list of lists with first binary interaction parameters
        for the Adachi-Sugie mixing rule.
    mijs : list of list of float
        N*N size list of lists with second binary interaction parameters
        for the Adachi-Sugie mixing rule.
    T : float, optional
        Temperature, [K]
    P : float, optional
        Pressure, [Pa]
    V : float, optional
        Molar volume, [m^3/mol]

    Examples
    --------
    T-P initialization, nitrogen-methane at 115 K and 1 MPa:

    >>> eos = PRASMIX(T=115, P=1E6, Tcs=[126.1, 190.6], Pcs=[33.94E5, 46.04E5], omegas=[0.04, 0.011], zs=[0.5, 0.5], lijs=[[0,0],[0,0]], mijs=[[0,0],[0,0]])
    >>> eos.V_l, eos.V_g
    (3.625735065042031e-05, 0.0007006656856469095)
    >>> eos.fugacities_l, eos.fugacities_g
    ([793860.8382114634, 73468.55225303846], [436530.9247009119, 358114.63827532396])

    Notes
    -----
    For P-V initializations, SciPy's `newton` solver is used to find T.

    References
    ----------
    .. [1] Peng, Ding-Yu, and Donald B. Robinson. "A New Two-Constant Equation
       of State." Industrial & Engineering Chemistry Fundamentals 15, no. 1
       (February 1, 1976): 59-64. doi:10.1021/i160057a011.
    .. [2] Robinson, Donald B., Ding-Yu Peng, and Samuel Y-K Chung. "The
       Development of the Peng - Robinson Equation and Its Application to Phase
       Equilibrium in a System Containing Methanol." Fluid Phase Equilibria 24,
       no. 1 (January 1, 1985): 25-41. doi:10.1016/0378-3812(85)87035-7.
    .. [3] Adachi Y., Sugie H. A new mixing rule-modified conventional mixing
       rule // Fluid Phase Equilib. 1986. Vol. 28, № 2. P. 103–118
    """
    a_alpha_mro = -5

    def __init__(self, Tcs, Pcs, omegas, zs, lijs=None, mijs=None,
                 T=None, P=None, V=None):
        GCASMIX.__init__(self, Tcs, zs, lijs=lijs, mijs=mijs)
        super(PRASMIX, self).__init__(Tcs, Pcs, omegas, zs, kijs=self.kijs,
                                      T=T, P=P, V=V)


class SRKASMIX(SRKMIX, GCASMIX):
    r"""Class for solving the Soave-Redlich-Kwong cubic equation of state for a
    mixture of any number of compounds. Subclasses `SRK`. Solves the EOS on
    initialization and calculates fugacities for all components in all phases.

    The implemented method here is `fugacity_coefficients`, which implements
    the formula for fugacity coefficients in a mixture as given in [1]_.
    Two of `T`, `P`, and `V` are needed to solve the EOS.

    This implementation uses Adachi-Sugie mixing rule.

    .. math::
        P = \frac{RT}{V-b} - \frac{a\alpha(T)}{V(V+b)}

        a \alpha = \sum_i \sum_j z_i z_j {(a\alpha)}_{ij}

        k_{ij} = l_{ij} + m_{ij} (z_i - z_j)

        (a\alpha)_{ij} = (1-k_{ij})\sqrt{(a\alpha)_{i}(a\alpha)_{j}}

        b = \sum_i z_i b_i

        a_i =\left(\frac{R^2(T_{c,i})^{2}}{9(\sqrt[3]{2}-1)P_{c,i}} \right)
        =\frac{0.42748\cdot R^2(T_{c,i})^{2}}{P_{c,i}}

        b_i =\left( \frac{(\sqrt[3]{2}-1)}{3}\right)\frac{RT_{c,i}}{P_{c,i}}
        =\frac{0.08664\cdot R T_{c,i}}{P_{c,i}}

        \alpha(T)_i = \left[1 + m_i\left(1 - \sqrt{\frac{T}{T_{c,i}}}\right)\right]^2

        m_i = 0.480 + 1.574\omega_i - 0.176\omega_i^2

    Parameters
    ----------
    Tcs : list of float
        Critical temperatures of all compounds, [K]
    Pcs : list of float
        Critical pressures of all compounds, [Pa]
    omegas : list of float
        Acentric factors of all compounds, [-]
    zs : list of float
        Overall mole fractions of all species, [-]
    lijs : list of list of float
        N*N size list of lists with first binary interaction parameters
        for the Adachi-Sugie mixing rule.
    mijs : list of list of float
        N*N size list of lists with second binary interaction parameters
        for the Adachi-Sugie mixing rule.
    T : float, optional
        Temperature, [K]
    P : float, optional
        Pressure, [Pa]
    V : float, optional
        Molar volume, [m^3/mol]

    Examples
    --------
    T-P initialization, nitrogen-methane at 115 K and 1 MPa:

    >>> eos = SRKASMIX(T=115, P=1E6, Tcs=[126.1, 190.6], Pcs=[33.94E5, 46.04E5], omegas=[0.04, 0.011], zs=[0.5, 0.5], lijs=[[0,0],[0,0]], mijs=[[0,0],[0,0]])
    >>> eos.V_l, eos.V_g
    (4.104755570185169e-05, 0.0007110155639819185)
    >>> eos.fugacities_l, eos.fugacities_g
    ([817841.6430546861, 72382.81925202614], [442137.12801246037, 361820.79211909405])

    Notes
    -----
    For P-V initializations, SciPy's `newton` solver is used to find T.

    References
    ----------
    .. [1] Soave, Giorgio. "Equilibrium Constants from a Modified Redlich-Kwong
       Equation of State." Chemical Engineering Science 27, no. 6 (June 1972):
       1197-1203. doi:10.1016/0009-2509(72)80096-4.
    .. [2] Poling, Bruce E. The Properties of Gases and Liquids. 5th
       edition. New York: McGraw-Hill Professional, 2000.
    .. [3] Walas, Stanley M. Phase Equilibria in Chemical Engineering.
       Butterworth-Heinemann, 1985.
    .. [4] Adachi Y., Sugie H. A new mixing rule-modified conventional mixing
       rule // Fluid Phase Equilib. 1986. Vol. 28, № 2. P. 103–118
    """
    a_alpha_mro = -5

    def __init__(self, Tcs, Pcs, omegas, zs, lijs=None, mijs=None,
                 T=None, P=None, V=None):
        GCASMIX.__init__(self, Tcs, zs, lijs=lijs, mijs=mijs)
        super(SRKASMIX, self).__init__(Tcs, Pcs, omegas, zs, kijs=self.kijs,
                                       T=T, P=P, V=V)


class PRSILMIX(PRMIX, PRSIL):
    r"""Class for solving the Peng-Robinson cubic equation of state for a
    mixture of any number of compounds. Subclasses `PR`. Solves the EOS on
    initialization and calculates fugacities for all components in all phases.

    The implemented method here is `fugacity_coefficients`, which implements
    the formula for fugacity coefficients in a mixture as given in [1]_.
    Two of `T`, `P`, and `V` are needed to solve the EOS.

    In this implementation constants for `a` and `b` calculated from
    critical compressibility factors of individual compounds.

    .. math::
        P = \frac{RT}{v-b}-\frac{a\alpha(T)}{v(v+b)+b(v-b)}

        a \alpha = \sum_i \sum_j z_i z_j {(a\alpha)}_{ij}

        (a\alpha)_{ij} = (1-k_{ij})\sqrt{(a\alpha)_{i}(a\alpha)_{j}}

        b = \sum_i z_i b_i

        \omega_{b_i} = (1 - \omega_{a_i}^\frac{1}{3})^\frac{3}{2}

        \omega_{b_i} = Zc_i - 1 + \omega_{a_i}^\frac{1}{3}

        a_i=\omega_{a_i}\frac{R^2T_{c,i}^2}{P_{c,i}}

        b_i=\omega_{b_i}\frac{RT_{c,i}}{P_{c,i}}

        \alpha(T)_i=[1+\kappa_i(1-\sqrt{T_{r,i}})]^2

        \kappa_i=0.37464+1.54226\omega_i-0.26992\omega^2_i

    Parameters
    ----------
    Tcs : list of float
        Critical temperatures of all compounds, [K]
    Pcs : list of float
        Critical pressures of all compounds, [Pa]
    Zcs : list of float
        Critical compressibility factors for all compounds.
    omegas : list of float
        Acentric factors of all compounds, [-]
    zs : list of float
        Overall mole fractions of all species, [-]
    kijs : list of list of float
        N*N size list of lists with binary interaction parameters
        for the Van der Waals mixing rule.
    T : float, optional
        Temperature, [K]
    P : float, optional
        Pressure, [Pa]
    V : float, optional
        Molar volume, [m^3/mol]

    Examples
    --------
    T-P initialization, nitrogen-methane at 115 K and 1 MPa:

    >>> eos = PRSILMIX(T=115, P=1E6, Tcs=[126.1, 190.6], Pcs=[33.94E5, 46.04E5], Zcs=[0.2895, 0.2862], omegas=[0.04, 0.011], zs=[0.5, 0.5], kijs=[[0,0],[0,0]])
    >>> eos.V_l, eos.V_g
    (4.171471041603432e-05, 0.0006546525130773197)
    >>> eos.fugacities_l, eos.fugacities_g
    ([754863.4134152527, 67834.25024347089], [431167.93422514194, 339247.96353764256])

    Notes
    -----
    For P-V initializations, SciPy's `newton` solver is used to find T.

    References
    ----------
    .. [1] Peng, Ding-Yu, and Donald B. Robinson. "A New Two-Constant Equation
       of State." Industrial & Engineering Chemistry Fundamentals 15, no. 1
       (February 1, 1976): 59-64. doi:10.1021/i160057a011.
    .. [2] Robinson, Donald B., Ding-Yu Peng, and Samuel Y-K Chung. "The
       Development of the Peng - Robinson Equation and Its Application to Phase
       Equilibrium in a System Containing Methanol." Fluid Phase Equilibria 24,
       no. 1 (January 1, 1985): 25-41. doi:10.1016/0378-3812(85)87035-7.
    .. [3] Suigie H., Iwahori Y. L.b.c.-y. On the Application of Cubic Equations
       of State: Analytical Expression for alpha/Tr and Improved Liquid Density
       Calculations // Fluid Phase Equilib. 1989. Vol. 50. P. 1–20.
    """
    def __init__(self, Tcs, Pcs, Zcs, omegas, zs,
                 kijs=None, T=None, P=None, V=None):
        self.N = len(Tcs)
        self.cmps = range(self.N)
        self.Tcs = Tcs
        self.Pcs = Pcs
        self.Zcs = Zcs
        self.omegas = omegas
        self.zs = zs
        if kijs is None:
            kijs = [[0] * self.N] * self.N
        self.kijs = kijs
        self.T = T
        self.P = P
        self.V = V

        self.c1s = []
        self.c2s = []
        for Zc in Zcs:
            c1, c2 = self.calculate_c1c2(Zc)
            self.c1s.append(c1)
            self.c2s.append(c2)

        self.ais = [c1*R*R*Tc*Tc/Pc for Tc, Pc, c1 in zip(Tcs, Pcs, self.c1s)]
        self.bs = [c2*R*Tc/Pc for Tc, Pc, c2 in zip(Tcs, Pcs, self.c2s)]
        self.b = sum(bi*zi for bi, zi in zip(self.bs, self.zs))
        self.kappas = [0.37464 + 1.54226*w - 0.26992*w*w for w in omegas]

        self.delta = 2.*self.b
        self.epsilon = -self.b*self.b

        self.solve()
        self.fugacities()


class SRKSILMIX(SRKMIX, SRKSIL):
    r"""Class for solving the Soave-Redlich-Kwong cubic equation of state for a
    mixture of any number of compounds. Subclasses `SRK`. Solves the EOS on
    initialization and calculates fugacities for all components in all phases.

    The implemented method here is `fugacity_coefficients`, which implements
    the formula for fugacity coefficients in a mixture as given in [1]_.
    Two of `T`, `P`, and `V` are needed to solve the EOS.

    In this implementation constants for `a` and `b` calculated from
    critical compressibility factors of individual compounds.

    .. math::
        P = \frac{RT}{V-b} - \frac{a\alpha(T)}{V(V+b)}

        a \alpha = \sum_i \sum_j z_i z_j {(a\alpha)}_{ij}

        (a\alpha)_{ij} = (1-k_{ij})\sqrt{(a\alpha)_{i}(a\alpha)_{j}}

        b = \sum_i z_i b_i

        \omega_{b_i} = (1 - \omega_{a_i}^\frac{1}{3})^\frac{3}{2}

        \omega_{b_i} = Zc_i - 1 + \omega_{a_i}^\frac{1}{3}

        a_i =\omega_{a_i}\frac{R^2 T_{c,i}^{2}}{P_{c,i}}

        b_i =\omega_{b_i}\frac{R T_{c,i}}{P_{c,i}}

        \alpha(T)_i = \left[1 + m_i\left(1 - \sqrt{\frac{T}{T_{c,i}}}\right)\right]^2

        m_i = 0.480 + 1.574\omega_i - 0.176\omega_i^2

    Parameters
    ----------
    Tcs : list of float
        Critical temperatures of all compounds, [K]
    Pcs : list of float
        Critical pressures of all compounds, [Pa]
    Zcs : list of float
        Critical compressibility factors for all compounds.
    omegas : list of float
        Acentric factors of all compounds, [-]
    zs : list of float
        Overall mole fractions of all species, [-]
    kijs : list of list of float
        N*N size list of lists with binary interaction parameters
        for the Van der Waals mixing rule.
    T : float, optional
        Temperature, [K]
    P : float, optional
        Pressure, [Pa]
    V : float, optional
        Molar volume, [m^3/mol]

    Examples
    --------
    T-P initialization, nitrogen-methane at 115 K and 1 MPa:

    >>> eos = SRKSILMIX(T=115, P=1E6, Tcs=[126.1, 190.6], Pcs=[33.94E5, 46.04E5], Zcs=[0.2895, 0.2862], omegas=[0.04, 0.011], zs=[0.5, 0.5], kijs=[[0,0],[0,0]])
    >>> eos.V_l, eos.V_g
    (3.882927576356822e-05, 0.0006047118329340757)
    >>> eos.fugacities_l, eos.fugacities_g
    ([537562.5485920205, 24215.278141803272], [432722.9307603221, 324745.15130377223])

    Notes
    -----
    For P-V initializations, SciPy's `newton` solver is used to find T.

    References
    ----------
    .. [1] Soave, Giorgio. "Equilibrium Constants from a Modified Redlich-Kwong
       Equation of State." Chemical Engineering Science 27, no. 6 (June 1972):
       1197-1203. doi:10.1016/0009-2509(72)80096-4.
    .. [2] Poling, Bruce E. The Properties of Gases and Liquids. 5th
       edition. New York: McGraw-Hill Professional, 2000.
    .. [3] Walas, Stanley M. Phase Equilibria in Chemical Engineering.
       Butterworth-Heinemann, 1985.
    .. [4] Suigie H., Iwahori Y. L.b.c.-y. On the Application of Cubic Equations
       of State: Analytical Expression for alpha/Tr and Improved Liquid Density
       Calculations // Fluid Phase Equilib. 1989. Vol. 50. P. 1–20.
    """
    def __init__(self, Tcs, Pcs, Zcs, omegas, zs, kijs=None,
                 T=None, P=None, V=None):
        self.N = len(Tcs)
        self.cmps = range(self.N)
        self.Tcs = Tcs
        self.Pcs = Pcs
        self.Zcs = Zcs
        self.omegas = omegas
        self.zs = zs
        if kijs is None:
            kijs = [[0]*self.N for i in range(self.N)]
        self.kijs = kijs
        self.T = T
        self.P = P
        self.V = V

        self.c1s = []
        self.c2s = []
        for Zc in Zcs:
            c1, c2 = self.calculate_c1c2(Zc)
            self.c1s.append(c1)
            self.c2s.append(c2)

        self.ais = [c1*R*R*Tc*Tc/Pc for Tc, Pc, c1 in zip(Tcs, Pcs, self.c1s)]
        self.bs = [c2*R*Tc/Pc for Tc, Pc, c2 in zip(Tcs, Pcs, self.c2s)]
        self.b = sum(bi*zi for bi, zi in zip(self.bs, self.zs))
        self.ms = [0.480 + 1.574*omega - 0.176*omega*omega for omega in omegas]
        self.delta = self.b

        self.solve()
        self.fugacities()


class PRASILMIX(PRSILMIX, GCASMIX):
    r"""Class for solving the Peng-Robinson cubic equation of state for a
    mixture of any number of compounds. Subclasses `PR`. Solves the EOS on
    initialization and calculates fugacities for all components in all phases.

    The implemented method here is `fugacity_coefficients`, which implements
    the formula for fugacity coefficients in a mixture as given in [1]_.
    Two of `T`, `P`, and `V` are needed to solve the EOS.

    In this implementation constants for `a` and `b` calculated from
    critical compressibility factors of individual compounds and
    uses Adachi-Sugie mixing rule.

    .. math::
        P = \frac{RT}{v-b}-\frac{a\alpha(T)}{v(v+b)+b(v-b)}

        a \alpha = \sum_i \sum_j z_i z_j {(a\alpha)}_{ij}

        k_{ij} = l_{ij} + m_{ij} (z_i - z_j)

        (a\alpha)_{ij} = (1-k_{ij})\sqrt{(a\alpha)_{i}(a\alpha)_{j}}

        b = \sum_i z_i b_i

        \omega_{b_i} = (1 - \omega_{a_i}^\frac{1}{3})^\frac{3}{2}

        \omega_{b_i} = Zc_i - 1 + \omega_{a_i}^\frac{1}{3}

        a_i=\omega_{a_i}\frac{R^2T_{c,i}^2}{P_{c,i}}

        b_i=\omega_{b_i}\frac{RT_{c,i}}{P_{c,i}}

        \alpha(T)_i=[1+\kappa_i(1-\sqrt{T_{r,i}})]^2

        \kappa_i=0.37464+1.54226\omega_i-0.26992\omega^2_i

    Parameters
    ----------
    Tcs : list of float
        Critical temperatures of all compounds, [K]
    Pcs : list of float
        Critical pressures of all compounds, [Pa]
    Zcs : list of float
        Critical compressibility factors for all compounds.
    omegas : list of float
        Acentric factors of all compounds, [-]
    zs : list of float
        Overall mole fractions of all species, [-]
    kijs : list of list of float
        N*N size list of lists with binary interaction parameters
        for the Van der Waals mixing rule.
    T : float, optional
        Temperature, [K]
    P : float, optional
        Pressure, [Pa]
    V : float, optional
        Molar volume, [m^3/mol]

    Examples
    --------
    T-P initialization, nitrogen-methane at 115 K and 1 MPa:

    >>> eos = PRASILMIX(T=115, P=1E6, Tcs=[126.1, 190.6], Pcs=[33.94E5, 46.04E5], Zcs=[0.2895, 0.2862], omegas=[0.04, 0.011], zs=[0.5, 0.5], lijs=[[0,0],[0,0]], mijs=[[0,0],[0,0]])
    >>> eos.V_l, eos.V_g
    (4.171471041603432e-05, 0.0006546525130773197)
    >>> eos.fugacities_l, eos.fugacities_g
    ([754863.4134152527, 67834.25024347089], [431167.93422514194, 339247.96353764256])

    Notes
    -----
    For P-V initializations, SciPy's `newton` solver is used to find T.

    References
    ----------
    .. [1] Peng, Ding-Yu, and Donald B. Robinson. "A New Two-Constant Equation
       of State." Industrial & Engineering Chemistry Fundamentals 15, no. 1
       (February 1, 1976): 59-64. doi:10.1021/i160057a011.
    .. [2] Robinson, Donald B., Ding-Yu Peng, and Samuel Y-K Chung. "The
       Development of the Peng - Robinson Equation and Its Application to Phase
       Equilibrium in a System Containing Methanol." Fluid Phase Equilibria 24,
       no. 1 (January 1, 1985): 25-41. doi:10.1016/0378-3812(85)87035-7.
    .. [3] Suigie H., Iwahori Y. L.b.c.-y. On the Application of Cubic Equations
       of State: Analytical Expression for alpha/Tr and Improved Liquid Density
       Calculations // Fluid Phase Equilib. 1989. Vol. 50. P. 1–20.
    .. [4] Adachi Y., Sugie H. A new mixing rule-modified conventional mixing
       rule // Fluid Phase Equilib. 1986. Vol. 28, № 2. P. 103–118
    """
    a_alpha_mro = -5

    def __init__(self, Tcs, Pcs, Zcs, omegas, zs,
                 lijs=None, mijs=None, T=None, P=None, V=None):
        GCASMIX.__init__(self, Tcs, zs, lijs=lijs, mijs=mijs)
        super(PRASILMIX, self).__init__(Tcs, Pcs, Zcs, omegas, zs,
                                        kijs=self.kijs, T=T, P=P, V=V)


class SRKASILMIX(SRKSILMIX, GCASMIX):
    r"""Class for solving the Soave-Redlich-Kwong cubic equation of state for a
    mixture of any number of compounds. Subclasses `SRK`. Solves the EOS on
    initialization and calculates fugacities for all components in all phases.

    The implemented method here is `fugacity_coefficients`, which implements
    the formula for fugacity coefficients in a mixture as given in [1]_.
    Two of `T`, `P`, and `V` are needed to solve the EOS.

    In this implementation constants for `a` and `b` calculated from
    critical compressibility factors of individual compounds and 
    uses Adachi-Sugie mixing rule.

    .. math::
        P = \frac{RT}{V-b} - \frac{a\alpha(T)}{V(V+b)}

        a \alpha = \sum_i \sum_j z_i z_j {(a\alpha)}_{ij}

        k_{ij} = l_{ij} + m_{ij} (z_i - z_j)

        (a\alpha)_{ij} = (1-k_{ij})\sqrt{(a\alpha)_{i}(a\alpha)_{j}}

        b = \sum_i z_i b_i

        \omega_{b_i} = (1 - \omega_{a_i}^\frac{1}{3})^\frac{3}{2}

        \omega_{b_i} = Zc_i - 1 + \omega_{a_i}^\frac{1}{3}

        a_i =\omega_{a_i}\frac{R^2 T_{c,i}^{2}}{P_{c,i}}

        b_i =\omega_{b_i}\frac{R T_{c,i}}{P_{c,i}}

        \alpha(T)_i = \left[1 + m_i\left(1 - \sqrt{\frac{T}{T_{c,i}}}\right)\right]^2

        m_i = 0.480 + 1.574\omega_i - 0.176\omega_i^2

    Parameters
    ----------
    Tcs : list of float
        Critical temperatures of all compounds, [K]
    Pcs : list of float
        Critical pressures of all compounds, [Pa]
    Zcs : list of float
        Critical compressibility factors for all compounds.
    omegas : list of float
        Acentric factors of all compounds, [-]
    zs : list of float
        Overall mole fractions of all species, [-]
    lijs : list of list of float
        N*N size list of lists with first binary interaction parameters
        for the Adachi-Sugie mixing rule.
    mijs : list of list of float
        N*N size list of lists with second binary interaction parameters
        for the Adachi-Sugie mixing rule.
    T : float, optional
        Temperature, [K]
    P : float, optional
        Pressure, [Pa]
    V : float, optional
        Molar volume, [m^3/mol]

    Examples
    --------
    T-P initialization, nitrogen-methane at 115 K and 1 MPa:

    >>> eos = SRKASILMIX(T=115, P=1E6, Tcs=[126.1, 190.6], Pcs=[33.94E5, 46.04E5], Zcs=[0.2895, 0.2862], omegas=[0.04, 0.011], zs=[0.5, 0.5], lijs=[[0,0],[0,0]], mijs=[[0,0],[0,0]])
    >>> eos.V_l, eos.V_g
    (3.882927576356822e-05, 0.0006047118329340757)
    >>> eos.fugacities_l, eos.fugacities_g
    ([537562.5485920205, 24215.278141803272], [432722.9307603221, 324745.15130377223])

    Notes
    -----
    For P-V initializations, SciPy's `newton` solver is used to find T.

    References
    ----------
    .. [1] Soave, Giorgio. "Equilibrium Constants from a Modified Redlich-Kwong
       Equation of State." Chemical Engineering Science 27, no. 6 (June 1972):
       1197-1203. doi:10.1016/0009-2509(72)80096-4.
    .. [2] Poling, Bruce E. The Properties of Gases and Liquids. 5th
       edition. New York: McGraw-Hill Professional, 2000.
    .. [3] Walas, Stanley M. Phase Equilibria in Chemical Engineering.
       Butterworth-Heinemann, 1985.
    .. [4] Suigie H., Iwahori Y. L.b.c.-y. On the Application of Cubic Equations
       of State: Analytical Expression for alpha/Tr and Improved Liquid Density
       Calculations // Fluid Phase Equilib. 1989. Vol. 50. P. 1–20.
    .. [5] Adachi Y., Sugie H. A new mixing rule-modified conventional mixing
       rule // Fluid Phase Equilib. 1986. Vol. 28, № 2. P. 103–118
    """
    a_alpha_mro = -5

    def __init__(self, Tcs, Pcs, Zcs, omegas, zs,
                 lijs=None, mijs=None, T=None, P=None, V=None):
        GCASMIX.__init__(self, Tcs, zs, lijs=lijs, mijs=mijs)
        super(SRKASILMIX, self).__init__(Tcs, Pcs, Zcs, omegas, zs,
                                         kijs=self.kijs, T=T, P=P, V=V)


def _wrap_method(vp, method):
    """Wrap VaporPressure method to get one-variable equation.
    Parameters
    ----------
    vp : thermo.vapor_pressure.VaporPressure
        VaporPressure class.
    method : str
        Name of the method to calculate saturated pressure.
    Returns
    -------
    Psat_eq : callable
        Temperature dependent equation for calculation of the
        saturated pressure.
    """
    def Psat_eq(T):
        return vp.calculate(T, method)
    return Psat_eq
