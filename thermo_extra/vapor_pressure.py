import os
import pandas as pd
from thermo_extra.dippr import yaws_Psat
from thermo import vapor_pressure

__all__ = ["VaporPressure", "ANTOINE_YAWS"]

folder = os.path.join(os.path.dirname(__file__), "VaporPressure")
antoine_yaws = pd.read_csv(os.path.join(folder, "AntoineYaws.csv"))

ANTOINE_YAWS = "ANTOINE_YAWS"
vapor_pressure.ANTOINE_YAWS = ANTOINE_YAWS
vapor_pressure.vapor_pressure_methods.append(ANTOINE_YAWS)
extra_methods = [ANTOINE_YAWS]


class VaporPressure(vapor_pressure.VaporPressure):
    def __init__(self, Tb=None, Tc=None, Pc=None, omega=None, CASRN="",
                 eos=None, name=""):
        super(VaporPressure, self).__init__(
            Tb=Tb,
            Tc=Tc,
            Pc=Pc,
            omega=omega,
            CASRN=CASRN,
            eos=eos
        )
        self.name = name

    def load_all_methods(self):
        super(VaporPressure, self).load_all_methods()
        data = antoine_yaws[antoine_yaws["cas"] == self.CASRN]
        if len(data) > 0:
            data = data.iloc[0]
        else:
            data = antoine_yaws[antoine_yaws["Name"] == self.name]
            if len(data) > 0:
                data = data.iloc[0]
            else:
                data = None
        if data is not None:
            *coefs, Tmin, Tmax = data[
                ["A", "B", "C", "D", "E", "Tmin", "Tmax"]
            ].values
            self.ANTOINE_YAWS_coefs = coefs
            self.all_methods.add(ANTOINE_YAWS)
            self.Tmin = min([self.Tmin, Tmin])
            self.Tmax = max([self.Tmax, Tmax])

    def calculate(self, T, method):
        if method in extra_methods:
            if method == ANTOINE_YAWS:
                Psat = yaws_Psat(T, *self.ANTOINE_YAWS_coefs)
        else:
            Psat = super(
                VaporPressure,
                self
            ).calculate(T, method)
        return Psat
