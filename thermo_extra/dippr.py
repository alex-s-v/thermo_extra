from numpy import log10
from scipy.constants import mmHg

__all__ = ["yaws_Psat"]


def yaws_Psat(T, A, B, C=0, D=0, E=0):
    r"""DIPPR Equation Yaws. Used for calculation vapor pressure
    of the individual substances.

    .. math::
        \log{P_{mmHg}} = A + \frac{B}{T} + C\cdot \lg T + D\cdot T + E\cdot T^2

        P_{Pa} = 10^\left(\log{P_{mmHg}}\right) \frac{101325}{760}

    Parameters
    ----------
    T : float
        Temperature [K].
    A-E : float
        Parameters of the equation A and B is required, other defaults to 0.

    Returns
    -------
    P : float
        Saturation pressure [Pa].
    """
    log10_p_mmHg = A + B/T + C*log10(T) + D*T + E*T**2
    return 10**log10_p_mmHg * mmHg
