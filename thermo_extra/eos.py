from thermo.eos import PR, SRK
from thermo.utils import R
from scipy.optimize import fsolve

__all__ = ["GSSIL", "PRSIL", "SRKSIL"]


class GSSIL(object):
    r"""Provides general functionality for calculation of
    Sugie-Iwahori-Lu coefficients used for obtaining 'a' and 'b'.

    .. math::

        \omega_b = (1 - \omega_a^\frac{1}{3})^\frac{3}{2}

        \omega_b = Zc - 1 + \omega_a^\frac{1}{3}

    References
    ----------
    .. [1] Suigie H., Iwahori Y. L.b.c.-y. On the Application of Cubic Equations
       of State: Analytical Expression for alpha/Tr and Improved Liquid Density
       Calculations // Fluid Phase Equilib. 1989. Vol. 50. P. 1–20.
    """

    def calculate_c1c2(self, Zc):
        """Calculate coefficients used for obtaining 'a' and 'b'

        Parameters
        ----------
        Zc : float
            Critical compressibility factor of the compound.

        Returns
        -------
        c1, c2: float
            Coefficients used for obtaining 'a' and 'b'.
        """
        c2 = fsolve(lambda x: Zc - x - x**(2/3), 0.5)[0]
        c1 = (1 - c2**(2/3))**3
        return c1, c2


class PRSIL(GSSIL, PR):
    r"""Class for solving the Peng-Robinson cubic
    equation of state for a pure compound. Subclasses `CUBIC_EOS`, which
    provides the methods for solving the EOS and calculating its assorted
    relevant thermodynamic properties. Solves the EOS on initialization.

    Implemented methods here are `a_alpha_and_derivatives`, which calculates
    a_alpha and its first and second derivatives, and `solve_T`, which from a
    specified `P` and `V` obtains `T`.

    This implementation uses Sugie-Iwahori-Lu method for calculation
    of coefficients used for obtaining 'a' and 'b'.

    Two of `T`, `P`, and `V` are needed to solve the EOS.

    .. math::
        P = \frac{RT}{v-b}-\frac{a\alpha(T)}{v(v+b)+b(v-b)}

        \omega_b = (1 - \omega_a^\frac{1}{3})^\frac{3}{2}

        \omega_b = Zc - 1 + \omega_a^\frac{1}{3}

        a=\omega_a\frac{R^2T_c^2}{P_c}

        b=\omega_b\frac{RT_c}{P_c}

        \alpha(T)=[1+\kappa(1-\sqrt{T_r})]^2

        \kappa=0.37464+1.54226\omega-0.26992\omega^2

    Parameters
    ----------
    Tc : float
        Critical temperature, [K]
    Pc : float
        Critical pressure, [Pa]
    Zc : float
        Critical compressibility factor of the compound.
    omega : float
        Acentric factor, [-]
    T : float, optional
        Temperature, [K]
    P : float, optional
        Pressure, [Pa]
    V : float, optional
        Molar volume, [m^3/mol]

    Examples
    --------
    T-P initialization, and exploring each phase's properties:

    >>> eos = PRSIL(Tc=591.75, Pc=4108000.0, Zc=0.2638, omega=0.257, T=400, P=1e5)
    >>> eos.V_l, eos.V_g
    (0.00011653111060311536, 0.03208340706118953)

    References
    ----------
    .. [1] Peng, Ding-Yu, and Donald B. Robinson. "A New Two-Constant Equation
       of State." Industrial & Engineering Chemistry Fundamentals 15, no. 1
       (February 1, 1976): 59-64. doi:10.1021/i160057a011.
    .. [2] Robinson, Donald B., Ding-Yu Peng, and Samuel Y-K Chung. "The
       Development of the Peng - Robinson Equation and Its Application to Phase
       Equilibrium in a System Containing Methanol." Fluid Phase Equilibria 24,
       no. 1 (January 1, 1985): 25-41. doi:10.1016/0378-3812(85)87035-7.
    .. [3] Privat, R., and J.-N. Jaubert. "PPR78, a Thermodynamic Model for the
       Prediction of Petroleum Fluid-Phase Behaviour," 11. EDP Sciences, 2011.
       doi:10.1051/jeep/201100011.
    .. [4] Suigie H., Iwahori Y. L.b.c.-y. On the Application of Cubic Equations
       of State: Analytical Expression for alpha/Tr and Improved Liquid Density
       Calculations // Fluid Phase Equilib. 1989. Vol. 50. P. 1–20.
    """
    def __init__(self, Tc, Pc, Zc, omega, T=None, P=None, V=None):
        self.Tc = Tc
        self.Pc = Pc
        self.Zc = Zc
        self.omega = omega
        self.T = T
        self.P = P
        self.V = V

        self.c1, self.c2 = self.calculate_c1c2(Zc)

        self.a = self.c1*R*R*Tc*Tc/Pc
        self.b = self.c2*R*Tc/Pc
        self.kappa = 0.37464 + 1.54226*omega - 0.26992*omega*omega
        self.delta = 2.*self.b
        self.epsilon = -self.b*self.b
        self.Vc = self.Zc*R*self.Tc/self.Pc
        self.kwargs = {"Zc": Zc}

        self.solve()


class SRKSIL(GSSIL, SRK):
    r"""Class for solving the Soave-Redlich-Kwong cubic
    equation of state for a pure compound. Subclasses `CUBIC_EOS`, which
    provides the methods for solving the EOS and calculating its assorted
    relevant thermodynamic properties. Solves the EOS on initialization.

    Implemented methods here are `a_alpha_and_derivatives`, which sets
    a_alpha and its first and second derivatives, and `solve_T`, which from a
    specified `P` and `V` obtains `T`.

    This implementation uses Sugie-Iwahori-Lu method for calculation
    of coefficients used for obtaining 'a' and 'b'.

    Two of `T`, `P`, and `V` are needed to solve the EOS.

    .. math::
        P = \frac{RT}{V-b} - \frac{a\alpha(T)}{V(V+b)}

        \omega_b = (1 - \omega_a^\frac{1}{3})^\frac{3}{2}

        \omega_b = Zc - 1 + \omega_a^\frac{1}{3}

        a=\omega_a\frac{R^2T_c^{2}}{P_c}

        b=\omega_b\frac{R T_c}{P_c}

        \alpha(T) = \left[1 + m\left(1 - \sqrt{\frac{T}{T_c}}\right)\right]^2

        m = 0.480 + 1.574\omega - 0.176\omega^2

    Parameters
    ----------
    Tc : float
        Critical temperature, [K]
    Pc : float
        Critical pressure, [Pa]
    Zc : float
        Critical compressibility factor of the compound.
    omega : float
        Acentric factor, [-]
    T : float, optional
        Temperature, [K]
    P : float, optional
        Pressure, [Pa]
    V : float, optional
        Molar volume, [m^3/mol]

    Examples
    --------
    >>> eos = SRKSIL(Tc=591.75, Pc=4108000.0, Zc=0.2638, omega=0.257, T=400, P=1e5)
    >>> eos.V_l, eos.V_g
    (0.00011333855566822888, 0.03202919496199218)

    References
    ----------
    .. [1] Soave, Giorgio. "Equilibrium Constants from a Modified Redlich-Kwong
       Equation of State." Chemical Engineering Science 27, no. 6 (June 1972):
       1197-1203. doi:10.1016/0009-2509(72)80096-4.
    .. [2] Poling, Bruce E. The Properties of Gases and Liquids. 5th
       edition. New York: McGraw-Hill Professional, 2000.
    .. [3] Walas, Stanley M. Phase Equilibria in Chemical Engineering.
       Butterworth-Heinemann, 1985.
    .. [4] Suigie H., Iwahori Y. L.b.c.-y. On the Application of Cubic Equations
       of State: Analytical Expression for alpha/Tr and Improved Liquid Density
       Calculations // Fluid Phase Equilib. 1989. Vol. 50. P. 1–20.
    """
    def __init__(self, Tc, Pc, Zc, omega, T=None, P=None, V=None):
        self.Tc = Tc
        self.Pc = Pc
        self.Zc = Zc
        self.omega = omega
        self.T = T
        self.P = P
        self.V = V

        self.c1, self.c2 = self.calculate_c1c2(Zc)

        self.a = self.c1*R*R*Tc*Tc/Pc
        self.b = self.c2*R*Tc/Pc
        self.m = 0.480 + 1.574*omega - 0.176*omega*omega
        self.Vc = self.Zc*R*self.Tc/self.Pc
        self.delta = self.b
        self.kwargs = {"Zc", Zc}

        self.solve()
