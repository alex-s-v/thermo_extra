"""
thermo-extra
============

Provides
    Adds new functionality to the Caleb Bell's thermo package.

Available subpackages
---------------------
dippr
    Some helpful general equations.
eos
    Representations of equations of state for the individual chemicals.
eos_mix
    Representations of equations of state for the mixtures.
vapor_pressure
    Calculation of saturation pressure using several methods.
vle
    Calculation of vapor-liquid equilibrium of mixtures using EoS.

Notes
-----

This package is mostly compatible with Caleb Bell's thermo package, but
in some cases, it's not the case because of my laziness or other factors.
"""
from . import dippr
from . import eos
from . import eos_mix
from . import vapor_pressure
from . import vle

from .dippr import *
from .eos import *
from .eos_mix import *
from .vapor_pressure import *
from .vle import *

__all__ = ["dippr", "eos", "eos_mix", "vapor_pressure", "vle"]

__all__.extend(dippr.__all__)
__all__.extend(eos.__all__)
__all__.extend(eos_mix.__all__)
__all__.extend(vapor_pressure.__all__)
__all__.extend(vle.__all__)

__version__ = "0.0.1"
